
import java.io.File;
import java.net.JarURLConnection;
import java.net.URL;
import org.apache.derby.jdbc.EmbeddedDataSource;
import org.mortbay.jetty.Connector;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.plus.naming.Resource;
import org.mortbay.jetty.nio.SelectChannelConnector;
import org.mortbay.jetty.webapp.WebAppContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * TODO fix or dead!
 * @author seiji
 */
public class Start {

    public static void main(String[] args) {
        Logger logger = LoggerFactory.getLogger(Start.class);

        try {
            
            logger.info("Program starting");
            logger.info("\tGroup ID : ${groupId}");
            logger.info("\tArtifact ID : ${artifactId}");
            logger.info("\tVersion : ${version}");

            logger.info("Starting Jetty server");

            Server server = new Server();
            logger.info("created server instance");

            int port = 8080;
            Connector connector = new SelectChannelConnector();
            connector.setPort(port);
            logger.info("set port as " + port);

            server.addConnector(connector);
            logger.info("added connector");

            // DataSourceを登録する
            logger.info("binding datasource");

            EmbeddedDataSource ds = new EmbeddedDataSource();
            logger.info("datasource class : " + ds.getClass().getName());

            String createDatabase = "true";
            String datasourceName = "embedded";
            String databaseName = System.getProperty("user.home") + "/data/${groupId}/${artifactId}/${version}/db;create=true";
            String jndiName = "jdbc/${artifactId}ds";

            ds.setCreateDatabase(createDatabase);
            logger.info("create database : " + createDatabase);

            ds.setDataSourceName(datasourceName);
            logger.info("datasource name : " + datasourceName);

            ds.setDatabaseName(databaseName);
            logger.info("database name : " + databaseName);

            Resource resource = new Resource(jndiName, ds);
            logger.info("data source is bound on " + jndiName);

            // 自分のはいっているWARファイル名を取得する
            logger.info("getting war file name of itself");

            URL url = Start.class.getClassLoader().getResource("Start.class");
            File warFile = new File(((JarURLConnection) url.openConnection()).getJarFile().getName());
            logger.info("war file name : " + warFile.getAbsolutePath());



            logger.info("mounting web application");
            
            WebAppContext context = new WebAppContext(warFile.getAbsolutePath(), "/${artifactId}");
            logger.info("web application is mounted at /${artifactId}");

            logger.info("setting up web application context");
            context.setConfigurationClasses(new String[]{
                    "org.mortbay.jetty.webapp.WebInfConfiguration",
                    "org.mortbay.jetty.plus.webapp.EnvConfiguration",
                    "org.mortbay.jetty.plus.webapp.Configuration",
                    "org.mortbay.jetty.webapp.JettyWebXmlConfiguration",
                    "org.mortbay.jetty.webapp.TagLibConfiguration"
                });
            logger.info("web application context is set up");

            logger.info("set web application context handler");
            server.setHandler(context);


            logger.info("server starting");
            server.start();

            logger.info("server joining");
            server.join();

        } catch (Exception ex) {
            logger.error("exception occurred", ex);
            ex.printStackTrace();
        }
    }
}
