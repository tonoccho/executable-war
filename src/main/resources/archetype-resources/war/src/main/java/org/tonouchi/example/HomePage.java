package org.tonouchi.example;

import org.apache.wicket.PageParameters;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HomePage extends WebPage {

    private Logger logger = LoggerFactory.getLogger(getClass());

    private static final long serialVersionUID = 1L;

    public HomePage(final PageParameters parameters) {
        logger.info("adding label");
        add(new Label("example", "Hello Executable WAR!"));

    }
}
