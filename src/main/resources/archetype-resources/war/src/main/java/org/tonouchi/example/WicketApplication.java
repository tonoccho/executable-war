package org.tonouchi.example;

import org.apache.wicket.protocol.http.WebApplication;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WicketApplication extends WebApplication {

    private EntityManager em;



    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    protected void init() {
        super.init();

        logger.info("creating entity manager");
        em = Persistence.createEntityManagerFactory("${groupId}.${artifactId}PU").createEntityManager();
    }

    public WicketApplication() {
        logger.info("constructor");
    }

    public Class getHomePage() {
        logger.info("returning homepage");
        return HomePage.class;
    }
    
}
