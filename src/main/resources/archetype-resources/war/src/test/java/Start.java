/*
 *  Copyright 2009 seiji.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
import org.mortbay.jetty.nio.SelectChannelConnector;
import org.apache.derby.jdbc.EmbeddedDataSource;
import org.mortbay.jetty.Connector;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.plus.naming.Resource;
import org.mortbay.jetty.webapp.WebAppContext;
/**
 * TODO fix or dead!
 * @author seiji
 */
public class Start {
    public static void main(String[] args) throws Exception{
        try {
            Server server = new Server();
            Connector connector = new SelectChannelConnector();
            connector.setPort(8080);
            server.addConnector(connector);

            // DataSourceを登録する
            EmbeddedDataSource ds = new EmbeddedDataSource();
            ds.setCreateDatabase("true");
            ds.setDataSourceName("embedded");
            ds.setDatabaseName(System.getProperty("user.home") + "/data/${groupId}/${artifactId}/${version}/db_test;create=true");
            Resource resource = new Resource("jdbc/${artifactId}ds", ds);

            WebAppContext context = new WebAppContext("src/main/webapp", "/${artifactId}");

            context.setConfigurationClasses(new String[]{
                    "org.mortbay.jetty.webapp.WebInfConfiguration",
                    "org.mortbay.jetty.plus.webapp.EnvConfiguration",
                    "org.mortbay.jetty.plus.webapp.Configuration",
                    "org.mortbay.jetty.webapp.JettyWebXmlConfiguration",
                    "org.mortbay.jetty.webapp.TagLibConfiguration"
                });

            server.setHandler(context);


            server.start();
            server.join();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
