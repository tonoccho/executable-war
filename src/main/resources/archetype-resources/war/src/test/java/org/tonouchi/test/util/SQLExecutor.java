package org.tonouchi.test.util;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * TODO fix or dead!
 * @author seiji
 */
public class SQLExecutor {

    public static void execute(String fileName) throws SQLException, FileNotFoundException, ClassNotFoundException, IOException {
        Connection con = null;
        try {
            LineNumberReader lnr = new LineNumberReader(new FileReader(fileName));

            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
            con =
                DriverManager.getConnection(
                "jdbc:derby:" +
                System.getProperty("user.home") +
                "/data/${groupId}/${artifactId}/${version}/db_test;create=true");

            Statement stmt = con.createStatement();

            String sql;
            while ((sql = lnr.readLine()) != null) {
                System.out.println("executing : " + sql);
                stmt.execute(sql);
            }

        } finally {
            if (con != null && !con.isClosed()) {
                con.close();
            }
        }

    }
}
