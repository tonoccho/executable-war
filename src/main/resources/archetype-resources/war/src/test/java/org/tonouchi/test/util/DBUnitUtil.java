package org.tonouchi.test.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

/**
 * DBを利用したテストを作成する時にこのクラスを継承する。
 * @author tonocchi
 */
public abstract class DBUnitUtil {

    /**
     * DBのドライバ名
     */
    private String driver = "org.apache.derby.jdbc.EmbeddedDriver";
    /**
     * DBのURL
     *
     */
    private String url = "jdbc:derby:" + System.getProperty("user.home") + "/data/${groupId}/${artifactId}/${version}/db_test;create=true";
    /**
     * DBUnitが利用するコネクション
     */
    private IDatabaseConnection connection = null;

    /**
     * DBUnit、JPAのコネクションを作成します。
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException
     */
    private void open() throws SQLException, ClassNotFoundException, DatabaseUnitException {
        Class.forName(driver);
        Connection con = DriverManager.getConnection(url);
        connection = new DatabaseConnection(con);
    }

    /**
     * DBUnit、JPAのコネクションを解放します
     * @throws java.sql.SQLException
     */
    private void close() throws SQLException {
        connection.close();
        connection = null;
    }

    /**
     * DBUnitを利用してXMLに定義されたデータをDBのテーブルに挿入します。
     * @param datasetFileName
     * @throws java.sql.SQLException
     * @throws java.io.FileNotFoundException
     * @throws java.io.IOException
     * @throws org.dbunit.dataset.DataSetException
     * @throws org.dbunit.DatabaseUnitException
     */
    public void readyData(String datasetFileName) throws SQLException, FileNotFoundException, IOException, DataSetException, DatabaseUnitException {
        IDataSet dataSet =
            new FlatXmlDataSet(new FileInputStream(datasetFileName));
        DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);
    }

    /**
     * DBUnitを利用してXMLに定義されたテーブルからデータをすべて削除します。
     * @param datasetFileName
     * @throws java.sql.SQLException
     * @throws java.io.FileNotFoundException
     * @throws java.io.IOException
     * @throws org.dbunit.dataset.DataSetException
     * @throws org.dbunit.DatabaseUnitException
     */
    public void cleanupData(String datasetFileName) throws SQLException, FileNotFoundException, IOException, DataSetException, DatabaseUnitException {
        IDataSet dataSet =
            new FlatXmlDataSet(new FileInputStream(datasetFileName));
        DatabaseOperation.DELETE_ALL.execute(connection, dataSet);
    }
}
